#Instalar Virtualbox
https://www.virtualbox.org/wiki/Downloads
#Intalar vagrant
https://www.vagrantup.com/downloads.html
#Crea carpeta para el proyecto(si la tienes en git mejor)
Tecnotiburonesparteuno
#Ahora inicia vagrant para que descargue las cajas de ubuntu 14
vagrant init "ubuntu/trusty64"
#Entrando en calor con Vagrant 
#levantemos 3 máquinas modificanto el archivo Vagranfile

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
# Use the same key for each machine
config.ssh.insert_key = false
config.vm.provider "virtualbox" do |v|
v.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
v.memory = 512
end
config.vm.define "Aplicacion" do |vagrant1|
vagrant1.vm.box = "ubuntu/trusty64"
vagrant1.vm.hostname ="aplicacion"
vagrant1.vm.network "private_network" , ip: "10.0.0.70"
end
config.vm.define "bd" do |vagrant2|
vagrant2.vm.box = "ubuntu/trusty64"
vagrant2.vm.network "private_network" , ip: "10.0.0.80"
vagrant2.vm.hostname ="bd"
end
config.vm.define "web" do |vagrant3|
vagrant3.vm.box = "ubuntu/trusty64"
vagrant3.vm.network "private_network" , ip: "10.0.0.90"
vagrant3.vm.hostname ="web" 
end
end

# La primera ingreso por ssh /la segunda la apago y luego la enciendo/ la tercera la elimino
#revisa el estado 
vagrant status
# Por último destruye el ambiente que creaste
vagrant ssh namemahine halt
vagrant suspend
vagrant halt
vagrant destroy
#con vagrant status revisa el estado de las maquinas 
# ahora creo otra carpeta con el nombre de tecnotiburonesparte2 y luego inicio vagrant
# luego copia los archivo de la plantilla html
git clone https://johndominguezre@bitbucket.org/johndominguezre/tecnotiburones.git
#Los archivos deben estar en la raíz de la carpeta
copia los archivos en la raíz
#ahora crea el cookbok este debe tener el siguiente esquema de carpetas
cookbooks-vagrant_la-recipes
#dentro de recipes crea archivo con el nombre default.rb con el sigiente contenido
execute "apt-get update"
package "apache2"
execute "rm -rf /var/www/html"
link "/var/www/html" do
        to "/vagrant"
end
# Este es nuestro primer cookbook (analizar)
#Ahora modificaremos nuestro archivo Vagrantfile para que use el cookbook
VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
# Use the same key for each machine
config.vm.provider "virtualbox" do |v|
  v.memory = 2000
  v.cpus = 2
end
config.ssh.insert_key = true
config.vm.define "chef" do |vagrant1|
vagrant1.vm.box = "ubuntu/trusty64"
vagrant1.vm.hostname ="chef"
vagrant1.vm.network "forwarded_port", guest: 80, host: 8080
config.vm.provision "chef_solo" do |chef|
       chef.add_recipe "vagrant_la"
end
end
end
#Ahora analiza cada paso y describelo
#Vagrant up 
#Si todo sale bien podremos ver la  plantilla html por el puerto 8080 que fue el que publicamos y listo



